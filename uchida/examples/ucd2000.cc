/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2021 Ryota Uchida
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Ryota Uchida <is0398pp@ed.ritsumei.ac.jp>
 *         Ritsumeikan University, Shiga, Japan
 */


#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/uchida-module.h"
#include "ns3/applications-module.h"
#include "ns3/netanim-module.h"

#include "ns3/ocb-wifi-mac.h"
#include "ns3/wifi-80211p-helper.h"
#include "ns3/wave-mac-helper.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include <time.h>

#include "ns3/point-to-point-module.h"
#include "ns3/random-direction-2d-mobility-model.h"


NS_LOG_COMPONENT_DEFINE ("UchidaProtocolMinimum");

using namespace ns3;


//uint16_t port = 625;
Ptr<ConstantPositionMobilityModel> cpmm;
Ptr<RandomDirection2dMobilityModel> rd2m;
//Ptr<ConstantVelocityMobilityModel> cvMob;
Ptr<SteadyStateRandomWaypointMobilityModel> ssr;


int
main (int argc, char *argv[])
{

   int numNodes = 16;
   int p2pN=1;
   int car=200;
   int truck =0;
   double heightField = 2000;
   double widthField = 2000;

   
   //int pktSize = 1024;  //packet Size in bytes


   RngSeedManager::SetSeed(15);
   RngSeedManager::SetRun (7);

   ////////////////////// CREATE NODES ////////////////////////////////////
   
   NodeContainer p2pnodes;
   p2pnodes.Create(p2pN);

   
   NodeContainer nodes;
   nodes.Create(numNodes);

   NodeContainer cars;
   cars.Create(car);

   NodeContainer trucks;
   trucks.Create(truck);

   std::cout<<"Nodes created\n";

   ///////////////////// CREATE DEVICES ////////////////////////////////////

    UintegerValue ctsThr = (true ? UintegerValue (100) : UintegerValue (2200));
   Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", ctsThr);

   // A high number avoid drops in packet due to arp traffic.
   Config::SetDefault ("ns3::ArpCache::PendingQueueSize", UintegerValue (400));

   PointToPointHelper p2p;
   p2p.SetDeviceAttribute("DataRate",StringValue("800Mbps"));
   p2p.SetChannelAttribute("Delay",StringValue("0.1ms"));

   NetDeviceContainer mleftR; //Router
   NetDeviceContainer mleftL; //Leaf

   for(int i=0;i<=15;i++){
   NetDeviceContainer c = p2p.Install(p2pnodes.Get(0),nodes.Get(i));
   mleftR.Add(c.Get(0));
   mleftL.Add(c.Get(1));
   }
  

  // wifi 11p settei!!!
  NqosWaveMacHelper wifi80211pMac = NqosWaveMacHelper::Default();
  //wifi80211pMac.SetType ("ns3::AdhocWifiMac");
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
    //YansWifiPhyHelper wifiPhy1 = YansWifiPhyHelper::Default ();
  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss("ns3::RangePropagationLossModel","MaxRange", DoubleValue(250));


    //通信範囲の設定（半径）
  wifiPhy.SetChannel (wifiChannel.Create ());
  Wifi80211pHelper wifi80211p = Wifi80211pHelper::Default();
  wifi80211p.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate6MbpsBW10MHz"), "RtsCtsThreshold", UintegerValue (0),"ControlMode",StringValue("OfdmRate6MbpsBW10MHz"));
  
  NetDeviceContainer devices;
  devices = wifi80211p.Install (wifiPhy, wifi80211pMac, nodes);  //RSU
  devices.Add (wifi80211p.Install (wifiPhy, wifi80211pMac, cars)); //car
   //devices.Add (wifi80211p.Install (wifiPhy, wifi80211pMac, trucks));
  
  //大型車
  YansWifiPhyHelper wifiPhy1= YansWifiPhyHelper::Default ();
  YansWifiChannelHelper wifiChannel1;
  wifiChannel1.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel1.AddPropagationLoss("ns3::RangePropagationLossModel","MaxRange", DoubleValue(500));
    //通信範囲の設定（半径）
  
  wifiPhy1.SetChannel (wifiChannel1.Create ());
  NetDeviceContainer dope; //comunication range = 500m
  dope = wifi80211p.Install (wifiPhy1, wifi80211pMac, trucks); //turck
  dope.Add(wifi80211p.Install (wifiPhy1, wifi80211pMac, nodes)); //RSU

  
  //大型車to vehicle
  YansWifiPhyHelper wifiPhy2= YansWifiPhyHelper::Default ();
  YansWifiChannelHelper wifiChannel2;
  wifiChannel2.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel2.AddPropagationLoss("ns3::RangePropagationLossModel","MaxRange", DoubleValue(500));
    //通信範囲の設定（半径）
  
  wifiPhy2.SetChannel (wifiChannel2.Create ());
  NetDeviceContainer dig; //375m(range)
  dig = wifi80211p.Install (wifiPhy2, wifi80211pMac, trucks);
  dig.Add(wifi80211p.Install (wifiPhy2, wifi80211pMac, cars));





/*
for(int i=1;i<=2;i++){
    if(i==1){
    wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
    wifiChannel.AddPropagationLoss("ns3::RangePropagationLossModel","MaxRange", DoubleValue(250*i));
    wifiPhy.SetChannel (wifiChannel.Create ());
    wifi80211p.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate6MbpsBW10MHz"), "RtsCtsThreshold", UintegerValue (0),"ControlMode",StringValue("OfdmRate6MbpsBW10MHz"));
    devices = wifi80211p.Install (wifiPhy, wifi80211pMac, nodes);  //RSU
    devices.Add (wifi80211p.Install (wifiPhy, wifi80211pMac, cars)); //car
    }else if(i==2){
    wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
    wifiChannel.AddPropagationLoss("ns3::RangePropagationLossModel","MaxRange", DoubleValue(250*i));
    wifiPhy.SetChannel(wifiChannel.Create ());
    devices.Add(wifi80211p.Install (wifiPhy, wifi80211pMac, trucks));    
    }
}
*/
  
  std::cout<<" wifi Devices installed\n";


   std::cout<<"Devices installed\n";

   ////////////////////////   MOBILITY  ///////////////////////////////////////////

   /////// 1- Random in a rectangle Topology

   Ptr<UniformRandomVariable> x = CreateObject<UniformRandomVariable>();
   x->SetAttribute ("Min", DoubleValue (0));
   x->SetAttribute ("Max", DoubleValue (widthField));


   Ptr<UniformRandomVariable> y = CreateObject<UniformRandomVariable>();
   y->SetAttribute ("Min", DoubleValue (0));
   y->SetAttribute ("Max", DoubleValue (heightField));




MobilityHelper mobility;


//SDN(kari)
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(p2pnodes.Get(0));
    cpmm = p2pnodes.Get(0)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(1250,2000,0));



//RSU16台 格子状
 uint8_t n=0;
 for(uint8_t i=0;i<=3;i++){
   for(uint8_t j=0;j<=3;j++){
      mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
      mobility.Install(nodes.Get(n));
      cpmm = nodes.Get(n)->GetObject<ConstantPositionMobilityModel> ();
      cpmm->SetPosition(Vector(j*500,i*500,0)); 
  n++;
  }
 }

 //車(計12０台)
//ランダムの生成
  uint16_t now = (uint16_t)time(0);
   srand(now);
   rand();
//x座標固定の分

 uint16_t carnum = 0;
//SRC id=17
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(cars.Get(carnum));
    cpmm = cars.Get(carnum)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(10,0,0));
    carnum++;
//DST id=18
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(cars.Get(carnum));
    cpmm = cars.Get(carnum)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(1499,1500,0));
    carnum++;


  for(uint8_t i=0;i<=23;i++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(cars.Get(carnum));
    cpmm = cars.Get(carnum)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(0,rand()%1500,0));
    carnum++;
  }

for(uint8_t i=0;i<=24;i++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(cars.Get(carnum));
    cpmm = cars.Get(carnum)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(500,rand()%1500,0));
    carnum++;
  }

  for(uint8_t i=0;i<=24;i++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(cars.Get(carnum));
    cpmm = cars.Get(carnum)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(1000,rand()%1500,0));
    carnum++;
  }

for(uint8_t i=0;i<=24;i++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(cars.Get(carnum));
    cpmm = cars.Get(carnum)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(1500,rand()%1500,0));
    carnum++;
  }

//y座標固定の分
  for(uint8_t i=0;i<=24;i++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(cars.Get(carnum));
    cpmm = cars.Get(carnum)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(rand()%1500,0,0));
    carnum++;
  }

for(uint8_t i=0;i<=24;i++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(cars.Get(carnum));
    cpmm = cars.Get(carnum)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(rand()%1500,500,0));
    carnum++;
  }

  for(uint8_t i=0;i<=24;i++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(cars.Get(carnum));
    cpmm = cars.Get(carnum)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(rand()%1500,1000,0));
    carnum++;
  }

for(uint8_t i=0;i<=23;i++){
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(cars.Get(carnum));
    cpmm = cars.Get(carnum)->GetObject<ConstantPositionMobilityModel> ();
    cpmm->SetPosition(Vector(rand()%1500,1500,0));
    carnum++;
  }




/*
//動く車両２台
for (uint i=1 ; i <=2; i++){
    mobility.SetMobilityModel("ns3::ConstantVelocityMobilityModel");
    mobility.Install(nodes.Get(i));
    cvMob = nodes.Get(i)->GetObject<ConstantVelocityMobilityModel>();
    cvMob->SetPosition(Vector(i*10,30,0));
    cvMob->SetVelocity(Vector(10,0,0));
 }
*/
  ///////// 2- Grid Topology
  /*
   Ptr<GridPositionAllocator> alloc =CreateObject<GridPositionAllocator>();
   alloc->SetMinX((m_widthField/12)/2); //initial position X for the grid
   alloc->SetMinY(0); // Initial position Y for the grid
   alloc->SetDeltaX(m_widthField/12); //12 nodes per row (space between nodes in the row)
   alloc->SetDeltaY(m_heightField/10); //10 nodes per column (space between nodes in column)
   alloc->SetLayoutType( GridPositionAllocator::COLUMN_FIRST);
   alloc->SetN(10); //1 column  10 nodes


  MobilityHelper mobilityFixedNodes;
  mobilityFixedNodes.SetPositionAllocator (alloc);
  mobilityFixedNodes.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityFixedNodes.Install (cars);  */


  std::cout<<"mobility set \n";


  ////////////////////////   INTERNET STACK /////////////////////////

   UchidaHelper uchidaProtocol;

   Ipv4ListRoutingHelper listrouting;
   listrouting.Add(uchidaProtocol, 10);

   InternetStackHelper internet;
   internet.SetRoutingHelper(listrouting);
   internet.Install (nodes);    //RSU
   internet.Install (p2pnodes); //SDN
   internet.Install (cars);  //car
   internet.Install (trucks); //truck

   Ipv4AddressHelper ipv4;
   NS_LOG_INFO ("Assign IP Addresses.");

   ipv4.SetBase("10.1.1.0", "255.255.255.0");

   Ipv4InterfaceContainer mleftLI; //LeafInterface
   Ipv4InterfaceContainer mleftRI; //RouterInterface

   
   for(int i=0;i<=15;i++){
      NetDeviceContainer ndc;
      ndc.Add(mleftL.Get (i));
      ndc.Add(mleftR.Get (i));
      Ipv4InterfaceContainer ifc = ipv4.Assign (ndc);
      mleftLI.Add(ifc.Get(0)); 
      mleftRI.Add(ifc.Get(1));
      ipv4.NewNetwork();
   }

  //車とRSU
   ipv4.SetBase ("10.1.17.0", "255.255.255.0");
   Ipv4InterfaceContainer interfaces;
   interfaces = ipv4.Assign (devices);

  //トラックとRSU
   ipv4.SetBase ("10.1.18.0", "255.255.255.0");
   Ipv4InterfaceContainer interface;
   interface = ipv4.Assign (dope);

  //車とトラック
   ipv4.SetBase ("10.1.19.0", "255.255.255.0");
   Ipv4InterfaceContainer inter;
   inter = ipv4.Assign (dig);

   std::cout<<"Internet Stack installed\n";

   
	//////////////////// APPLICATIONS ////////////////////////////////
/*	uint16_t sinkPort = 9;
	PacketSinkHelper sink ("ns3::UdpSocketFactory",
	InetSocketAddress (Ipv4Address::GetAny (),sinkPort));
	ApplicationContainer sinkApps = sink.Install (mobileNode);
	sinkApps.Start (Seconds (158));
	sinkApps.Stop (Seconds (485));


	Ipv4Address sinkAddress = mobileNode->GetObject<Ipv4>()->GetAddress(1,0).GetLocal();
	uint16_t onoffPort = 9;
	OnOffHelper onoff1 ("ns3::UdpSocketFactory", InetSocketAddress (sinkAddress, onoffPort));
	onoff1.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"));
	onoff1.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0.0]"));
	onoff1.SetAttribute ("PacketSize", UintegerValue (pktSize));
	onoff1.SetAttribute ("DataRate", StringValue (std::to_string(appDataRate)));



	double deviation = 0;
	for(int i=0; i<=clientNodes.GetN()-1;  i++)
	  {
	     ApplicationContainer apps1;
		 apps1.Add(onoff1.Install (NodeList::GetNode (i)));
		 apps1.Start (Seconds (161)+Seconds(deviation));
		 apps1.Stop (Seconds (162)+Seconds(deviation));
		 deviation = deviation + 0.1;
	  }
    std::cout<<"applications installed\n";
/////////////////////////////////////////////////////////////////////////

*/



    AnimationInterface anim("animation5_0.xml");
 anim.SetMaxPktsPerTraceFile(99999999999);

	Simulator::Stop(Seconds (30));

	Simulator::Run ();


    // clean variables
    nodes = NodeContainer();
    p2pnodes = NodeContainer();
    cars = NodeContainer();
   


    Simulator::Destroy ();
    std::cout<<"end of simulation\n";
}
