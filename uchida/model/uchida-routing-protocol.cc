/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 IITP RAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

//(コメント)提案手法に大きく関わってくるファイル

#define NS_LOG_APPEND_CONTEXT                                   \
  if (m_ipv4) { std::clog << "[node " << m_ipv4->GetObject<Node> ()->GetId () << "] "; }

#include "uchida-routing-protocol.h"
#include "ns3/log.h"
#include "ns3/boolean.h"
#include "ns3/random-variable-stream.h"
#include "ns3/inet-socket-address.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/udp-l4-protocol.h"
#include "ns3/udp-header.h"
#include "ns3/wifi-net-device.h"
#include "ns3/adhoc-wifi-mac.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include <algorithm>
#include <limits>

#include <math.h>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <random>
#include <time.h>
#include <map>

#include "ns3/mobility-module.h"
#include "ns3/mobility-helper.h"
#include "ns3/vector.h"

#include <stdio.h>
#include <chrono>

#define INF 10000000
#define SIZE 100
#define TRUE 1
#define FALSE 0

int DIST[SIZE][SIZE];
int COST[SIZE];
int VIA[SIZE];
int N = 16;
char USED[SIZE];


uint16_t car = 200+16; //車の台数

int sendcount =0;
uint32_t ST;
uint32_t RT;
 

uint16_t cb2=0;
  



int dijkstra(int start, int goal)
{
  int min, target;

  COST[start] = 0;

  while(1){

    /* 未確定の中から距離が最も小さい地点(a)を選んで、その距離を その地点の最小距離として確定します */
    min = INF;
    for(int i = 0; i < N; i++){
      if(!USED[i] && min > COST[i]) {
        min = COST[i];
        target = i;
      }
    }

    /* 全ての地点の最短経路が確定 */
    if(target == goal)
      return COST[goal];

    /* 今確定した場所から「直接つながっている」かつ「未確定の」地点に関して、
    今確定した場所を経由した場合の距離を計算し、今までの距離よりも小さければ書き直します。 */
    for(int neighboring = 0; neighboring < N; neighboring++){
      if(COST[neighboring] > DIST[target][neighboring] + COST[target]) {
        COST[neighboring] = DIST[target][neighboring] + COST[target];
        VIA[neighboring] = target;
      }
    }
    USED[target] = TRUE;
  }
}



int roadD[24]={}; //{}で全部ゼロで初期化される！
int ucd=0; //roadDの初期化用


int count=0; //RSUから送られてきた回数（16機から受け取ったか確認する）
int path[7]={};

int recvcount[300]={};

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("UchidaUchidaRoutingProtocol");

namespace uchida {
NS_OBJECT_ENSURE_REGISTERED (UchidaRoutingProtocol);

/// UDP Port for UCHIDA control traffic
const uint32_t UchidaRoutingProtocol::UCHIDA_PORT = 654;
int posx = 0; //送信者の位置情報
int posy = 0;
int maxLenge = 0; // 受信者との距離マックス

Vector position[] = {};


UchidaRoutingProtocol::UchidaRoutingProtocol ()
{
  
}

TypeId
UchidaRoutingProtocol::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::uchida::UchidaRoutingProtocol")
    .SetParent<Ipv4RoutingProtocol> ()
    .SetGroupName ("Uchida")
    .AddConstructor<UchidaRoutingProtocol> ()
    .AddAttribute ("UniformRv",
                   "Access to the underlying UniformRandomVariable",
                   StringValue ("ns3::UniformRandomVariable"),
                   MakePointerAccessor (&UchidaRoutingProtocol::m_uniformRandomVariable),
                   MakePointerChecker<UniformRandomVariable> ())
  ;
  return tid;
}



UchidaRoutingProtocol::~UchidaRoutingProtocol ()
{
//(コメント)中身空っぽでもこの関数
}

void
UchidaRoutingProtocol::DoDispose ()
{
//(コメント)最後に呼ばれる関数
//この関数は空っぽでも必要
}

void
UchidaRoutingProtocol::PrintRoutingTable (Ptr<OutputStreamWrapper> stream, Time::Unit unit) const
{
  *stream->GetStream () << "Node: " << m_ipv4->GetObject<Node> ()->GetId ()
                        << "; Time: " << Now ().As (unit)
                        << ", Local time: " << GetObject<Node> ()->GetLocalTime ().As (unit)
                        << ", UCHIDA Routing table" << std::endl;

  //Print routing table here.
  *stream->GetStream () << std::endl;
//(コメント)Ipv4RoutingProtocolを使用しているなら空っぽでもこの関数は必要だけどあまり使わない関数
}

int64_t
UchidaRoutingProtocol::AssignStreams (int64_t stream)
{
  NS_LOG_FUNCTION (this << stream);
  m_uniformRandomVariable->SetStream (stream);
  return 1;
//(コメント)多分空っぽでも必要だけどあまり使わない関数
}



Ptr<Ipv4Route>
UchidaRoutingProtocol::RouteOutput (Ptr<Packet> p, const Ipv4Header &header,
                              Ptr<NetDevice> oif, Socket::SocketErrno &sockerr)
{

  std::cout<<"Route Ouput Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
  Ptr<Ipv4Route> route;

  if (!p)
    {
	  std::cout << "loopback occured! in routeoutput";
	  return route;// LoopbackRoute (header,oif);

	}

  if (m_socketAddresses.empty ())
    {
	  sockerr = Socket::ERROR_NOROUTETOHOST;
	  NS_LOG_LOGIC ("No zeal interfaces");
	  std::cout << "RouteOutput No zeal interfaces!!, packet drop\n";

	  Ptr<Ipv4Route> route;
	  return route;
    }





  
  return route;

//(コメント)同じく必要な関数
}



bool
UchidaRoutingProtocol::RouteInput (Ptr<const Packet> p, const Ipv4Header &header,
                             Ptr<const NetDevice> idev, UnicastForwardCallback ucb,
                             MulticastForwardCallback mcb, LocalDeliverCallback lcb, ErrorCallback ecb)
{
 
  std::cout<<"Route Input Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
  return true;
//(コメント)同じく必要な関数
}



void
UchidaRoutingProtocol::SetIpv4 (Ptr<Ipv4> ipv4)
{
  NS_ASSERT (ipv4 != 0);
  NS_ASSERT (m_ipv4 == 0);
  m_ipv4 = ipv4;
//(コメント)同じく必要な関数
}

void
UchidaRoutingProtocol::NotifyInterfaceUp (uint32_t i)
{
  NS_LOG_FUNCTION (this << m_ipv4->GetAddress (i, 0).GetLocal ()
                        << " interface is up");
  Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  Ipv4InterfaceAddress iface = l3->GetAddress (i,0);
  if (iface.GetLocal () == Ipv4Address ("127.0.0.1"))
    {
      return;
    }
  // Create a socket to listen only on this interface
  Ptr<Socket> socket;

  socket = Socket::CreateSocket (GetObject<Node> (),UdpSocketFactory::GetTypeId ());
  NS_ASSERT (socket != 0);
  socket->SetRecvCallback (MakeCallback (&UchidaRoutingProtocol::RecvSample,this));
  socket->BindToNetDevice (l3->GetNetDevice (i));
  socket->Bind (InetSocketAddress (iface.GetLocal (), UCHIDA_PORT));
  socket->SetAllowBroadcast (true);
  socket->SetIpRecvTtl (true);
  m_socketAddresses.insert (std::make_pair (socket,iface));


    // create also a subnet broadcast socket
  socket = Socket::CreateSocket (GetObject<Node> (),
                                 UdpSocketFactory::GetTypeId ());
  NS_ASSERT (socket != 0);
  socket->SetRecvCallback (MakeCallback (&UchidaRoutingProtocol::RecvSample, this));
  socket->BindToNetDevice (l3->GetNetDevice (i));
  socket->Bind (InetSocketAddress (iface.GetBroadcast (), UCHIDA_PORT));
  socket->SetAllowBroadcast (true);
  socket->SetIpRecvTtl (true);
  m_socketSubnetBroadcastAddresses.insert (std::make_pair (socket, iface));


  if (m_mainAddress == Ipv4Address ())
    {
      m_mainAddress = iface.GetLocal ();
    }

  NS_ASSERT (m_mainAddress != Ipv4Address ());


/*  for (uint32_t i = 0; i < m_ipv4->GetNInterfaces (); i++)
        {

          // Use primary address, if multiple
          Ipv4Address addr = m_ipv4->GetAddress (i, 0).GetLocal ();
        //  std::cout<<"############### "<<addr<<" |ninterface "<<m_ipv4->GetNInterfaces ()<<"\n";
       

              TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
              Ptr<Node> theNode = GetObject<Node> ();
              Ptr<Socket> socket = Socket::CreateSocket (theNode,tid);
              InetSocketAddress inetAddr (m_ipv4->GetAddress (i, 0).GetLocal (), SAMPLE_PORT);
              if (socket->Bind (inetAddr))
                {
                  NS_FATAL_ERROR ("Failed to bind() ZEAL socket");
                }
              socket->BindToNetDevice (m_ipv4->GetNetDevice (i));
              socket->SetAllowBroadcast (true);
              socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvSample, this));
              //socket->SetAttribute ("IpTtl",UintegerValue (1));
              socket->SetRecvPktInfo (true);

              m_socketAddresses[socket] = m_ipv4->GetAddress (i, 0);

              //  NS_LOG_DEBUG ("Socket Binding on ip " << m_mainAddress << " interface " << i);

              break;
           
        }
*/
//(コメント)なにしてるかわからんけど必要な関数
}

void
UchidaRoutingProtocol::NotifyInterfaceDown (uint32_t i)
{
//(コメント)同じく必要だけどあまり使わない関数  
}

void
UchidaRoutingProtocol::NotifyAddAddress (uint32_t i, Ipv4InterfaceAddress address)
{
//(コメント)同じく必要だけどあまり使わない関数 
}

void
UchidaRoutingProtocol::NotifyRemoveAddress (uint32_t i, Ipv4InterfaceAddress address)
{
//(コメント)同じく必要だけどあまり使わない関数 
}



void
UchidaRoutingProtocol::DoInitialize (void)
{

  //(コメント)最初に呼ばれる関数

      //printf("id=%d",id);    
      //std::cout<<"broadcast will be send\n";
      
      uint16_t id = m_ipv4->GetObject<Node> ()->GetId ();
      int i=0;
    for(i=0;i<=19;i++){
      Simulator::Schedule(Seconds(i), &UchidaRoutingProtocol::SendHello, this); 
      Simulator::Schedule(Seconds(i+0.52), &UchidaRoutingProtocol::SendSDN, this); 
    }

    if(id== 0){
      Simulator::Schedule(Seconds(21), &UchidaRoutingProtocol:: SimulationResult, this); 
    }



  /*
  if(id > 16){
        SendHello();
        Simulator::Schedule(Seconds(1), &UchidaRoutingProtocol::SendHello, this); 
        Simulator::Schedule(Seconds(2), &UchidaRoutingProtocol::SendHello, this); 
        Simulator::Schedule(Seconds(3), &UchidaRoutingProtocol::SendHello, this); 
        Simulator::Schedule(Seconds(4), &UchidaRoutingProtocol::SendHello, this); 
        Simulator::Schedule(Seconds(5), &UchidaRoutingProtocol::SendHello, this); 
        Simulator::Schedule(Seconds(6), &UchidaRoutingProtocol::SendHello, this); 

      }

      if(1<=id && id <=16){
         Simulator::Schedule(Seconds(0.6), &UchidaRoutingProtocol::SendSDN, this); 
         Simulator::Schedule(Seconds(1.6), &UchidaRoutingProtocol::SendSDN, this); 
         Simulator::Schedule(Seconds(2.6), &UchidaRoutingProtocol::SendSDN, this);
         Simulator::Schedule(Seconds(3.6), &UchidaRoutingProtocol::SendSDN, this); 
         Simulator::Schedule(Seconds(4.6), &UchidaRoutingProtocol::SendSDN, this); 
         Simulator::Schedule(Seconds(5.6), &UchidaRoutingProtocol::SendSDN, this); 
         Simulator::Schedule(Seconds(6.6), &UchidaRoutingProtocol::SendSDN, this); 
      }
  */  
/*
      if(id > 16){
        SendHello();
        for(int i=1;i<=10;i++){
         Simulator::Schedule(Seconds(i), &UchidaRoutingProtocol::SendHello, this);
        }
      }

      if(1<=id && id <=16){
         Simulator::Schedule(Seconds(0.6), &UchidaRoutingProtocol::SendSDN, this);
        for(int j=1;j<=10;j++){
        Simulator::Schedule(Seconds(j+0.6), &UchidaRoutingProtocol::SendSDN, this);
        }      
      }

        if(id==17){
         Simulator::Schedule(Seconds(0.65), &UchidaRoutingProtocol::SendBroadcast, this); 
         Simulator::Schedule(Seconds(2.65), &UchidaRoutingProtocol::SendBroadcast, this); 
      }

*/



}


void UchidaRoutingProtocol::RecvSample (Ptr<Socket> socket)
{
//(コメント)ブロードキャストを受信すると呼び出される関数
  //std::cout<<"In recv Sample(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
  
  Address sourceAddress;
  Ptr<Packet> packet = socket->RecvFrom (sourceAddress);
  InetSocketAddress inetSourceAddr = InetSocketAddress::ConvertFrom (sourceAddress);
  Ipv4Address sender = inetSourceAddr.GetIpv4 ();
  Ipv4Address receiver;

if (m_socketAddresses.find (socket) != m_socketAddresses.end ())
    receiver = m_socketAddresses[socket].GetLocal ();
    

  TypeHeader tHeader (SAMPLETYPE_RREP);
  packet->RemoveHeader (tHeader);

switch(tHeader.Get()){
    case HELLOTYPE:
      RecvHello(packet,receiver,sender);
      break;
    case SDNTYPE:
      RecvSDN(packet,receiver,sender);
      break;
    case STRTYPE:
      RecvSTR(packet,receiver,sender);
      break;
    case RTCTYPE:
      RecvRTC(packet,receiver,sender);
      break;
    case BROADCASTTYPE:
      RecvBroadcast(packet,receiver,sender);
      break;
    case BROADCAST2TYPE:
      RecvBroadcast2(packet,receiver,sender);
    default:
     // std::cout << "Unknown Type received in " << receiver << " from " << sender << "\n";
      break;

  }  


  
}


void
UchidaRoutingProtocol::SendXBroadcast (void)
{
//(コメント)ブロードキャストの設定&送信
 for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
       != m_socketAddresses.end (); ++j)
    {
      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      Ptr<Packet> packet = Create<Packet> ();


      RrepHeader rrepHeader(0,3,Ipv4Address("10.1.1.15"),5,Ipv4Address("10.1.1.13"),Seconds(3)); //(コメント)packet.cc,packet.hで作成したものを基準
                                                                                                 //に内容をパケット内に入れていく
      packet->AddHeader (rrepHeader);
      
      TypeHeader tHeader (SAMPLETYPE_RREP);
      packet->AddHeader (tHeader);
       
      // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
      Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        {
          destination = iface.GetBroadcast ();
        }
      socket->SendTo (packet, 0, InetSocketAddress (destination, UCHIDA_PORT));
      std::cout<<"broadcast sent\n"; 
        
     }
}




void
UchidaRoutingProtocol::SendHello()
{
  //printf("SendHello\n");
  //std::cout<<"In sendhello(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";

if(m_ipv4->GetObject<Node> ()->GetId () < 17){
  return;
}
  
  for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
    {

      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
      Vector pos = mobility->GetPosition ();
      HelloHeader helloHeader;
      Ptr<Packet> packet = Create<Packet> ();
      helloHeader.SetNodeId(m_ipv4->GetObject<Node> ()->GetId ());
      helloHeader.SetPosition_X(pos.x);
      helloHeader.SetPosition_Y(pos.y);
      packet->AddHeader(helloHeader);

      TypeHeader tHeader (HELLOTYPE);
      packet->AddHeader (tHeader);

    Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        { 
          destination = iface.GetBroadcast ();
        }


        //std::cout<<m_ipv4->GetObject<Node> ()->GetId ()<<"::::"<<destination<<"\n";


        /*
        destinationをいじれば送りたい目的をいじることができるみたい
        else if(m_ipv4->GetObject<Node> ()->GetId ()==1)
        { 
          destination = Ipv4Address ("10.1.1.255");
          //destination = iface.GetBroadcast ();
        }

        destination = iface.GetBroadcast ();で全部！！！

        std::cout<<destination<<"\n";
        std::cout<<"送信者のID："<<m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
        */
      
       //socket->SendTo (packet, 0, InetSocketAddress (destination, UCHIDA_PORT));

      Time jitter = Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0,500)));
      Simulator::Schedule (jitter, &UchidaRoutingProtocol::SendTo, this , socket, packet, destination);

    }  

  

}

void 
UchidaRoutingProtocol::SendTo(Ptr<Socket> socket, Ptr<Packet> packet, Ipv4Address destination)
    {
       //printf("SendTo\n");
      socket->SendTo (packet, 0, InetSocketAddress (destination, UCHIDA_PORT));
    }


void
UchidaRoutingProtocol::RecvHello(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
{
  //printf("RecvHello\n");
  //std::cout<<"In recvhello(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
  HelloHeader helloHeader;
  packet->RemoveHeader(helloHeader);
  uint16_t nodeId = helloHeader.GetNodeId();
  uint16_t pos_x = helloHeader.GetPosition_X();
  uint16_t pos_y = helloHeader.GetPosition_Y();


 //ここから250m以上で普通車の通信をやめさせるようにしてる
  uint16_t rid = m_ipv4->GetObject<Node> ()->GetId (); 

  if(rid>16){
    return;
  }
  Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
  Vector mypos = mobility->GetPosition ();
  uint16_t rpos_x;
  uint16_t rpos_y;
  rpos_x = mypos.x;
  rpos_y = mypos.y;
  double tmp = rpos_x;
  double tmp2 =rpos_y;
  tmp -= pos_x;
  tmp2 -= pos_y;
  //std::cout<<tmp<<"\n";
  //std::cout<<tmp2<<"\n";
  tmp=tmp*tmp;
  tmp2=tmp2*tmp2;
  double tmp3 = tmp +tmp2;
  double tmp4 = sqrt(tmp3);

if(tmp4>=250 && nodeId>16 && nodeId<=car){
  /*printf("aaaaaaaaaaaaaaaaaaaa\n");
  if(rid<=100 && rid>16){
     std::cout<<"受信者のID："<<rid<<"受信した位置("<< rpos_x<<","<<rpos_y<<")\n";
      std::cout<<"送信者のID："<<nodeId<<"送信した位置("<< pos_x<<","<<pos_y<<")\n";
  }      
  std::cout<<tmp4<<"\n";
  printf("aaaaaaaaaaaaaaaaa\n");
 */
 return;
}


//ここまで

/*
  車
  if(rid<=100 && rid>16){
     std::cout<<"受信者のID："<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
      std::cout<<"送信者のID："<<nodeId<<"送信した位置("<< pos_x<<","<<pos_y<<")\n";
      //std::cout<<"送信者のID："<<nodeId<<"\n";
  }    
  RSUが受け取ってるか確認
   if(rid<=16 && rid>0){
     std::cout<<"受信者のID："<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
      std::cout<<"送信者のID："<<nodeId<<"送信した位置("<< pos_x<<","<<pos_y<<")\n";
      //std::cout<<"送信者のID："<<nodeId<<"\n";
  }  

*/ 



  if(ucd==0){
    for(int i=0;i<=24;i++)
    {
      roadD[i]=1000;
    }
    ucd++;
  }

  //道ごとに車両の数を格納
if(rid == 1){      
      if(0<=pos_x && pos_x<499 && 0<=pos_y && pos_y<=1){
        if(nodeId>car){
           roadD[0]-=2;
        }
        roadD[0]-=1;
      }else if(0<=pos_x &&pos_x<=1 && 0<=pos_y && pos_y<499){
         if(nodeId>car){
           roadD[12]-=2;
        }
        roadD[12]-=1;
      }
  }else if(rid == 2){
      if(0<=pos_x && pos_x<499 && 0<=pos_y &&pos_y<=1){
         if(nodeId>car){
           roadD[0]-=2;
        }
        roadD[0]-=1;
      }else if(499<=pos_x && pos_x<999 && 0<=pos_y && pos_y<=1){
         if(nodeId>car){
           roadD[1]-=2;
        }
        roadD[1]-=1;
      }else if(499<=pos_x && pos_x<=500 && 0<=pos_y && pos_y<499){
         if(nodeId>car){
           roadD[13]-=2;
        }
        roadD[13]-=1;   
      }
  }else if(rid == 3){
      if(499<=pos_x && pos_x<999 && 0<=pos_y && pos_y<=1){
         if(nodeId>car){
           roadD[1]-=2;
        }
        roadD[1]-=1;
      }else if(999<=pos_x && pos_x<1499 && 0<=pos_y && pos_y<=1){
         if(nodeId>car){
           roadD[2]-=2;
        }
        roadD[2]-=1;
      }else if(999<=pos_x && pos_x<=1000 && 0<=pos_y && pos_y<499){
         if(nodeId>car){
           roadD[14]-=2;
        }
        roadD[14]-=1;   
      }
  }else if(rid == 4){      
      if(999<=pos_x && pos_x<=1500 && 0<=pos_y && pos_y<=1){
         if(nodeId>car){
           roadD[2]-=2;
        }
        roadD[2]-=1;
      }else if(1499<=pos_x && pos_x<=1500 && 0<=pos_y && pos_y<499){
         if(nodeId>car){
           roadD[15]-=2;
        }
        roadD[15]-=1;
      }
  }else if(rid == 5){
      if(0<=pos_x && pos_x<499 && 499<=pos_y && pos_y<=500){
         if(nodeId>car){
           roadD[3]-=2;
        }
        roadD[3]-=1;
      }else if(0<=pos_x && pos_x<=1 && 0<=pos_y && pos_y<499){
         if(nodeId>car){
           roadD[12]-=2;
        }
        roadD[12]-=1;
      }else if(0<=pos_x && pos_x<=1 && 499<=pos_y && pos_y<999){
         if(nodeId>car){
           roadD[16]-=2;
        }
        roadD[16]-=1;   
      }
  }else if(rid == 6){
      if(0<=pos_x && pos_x<499 && 499<=pos_y && pos_y<=500){
         if(nodeId>car){
           roadD[3]-=2;
        }
        roadD[3]-=1;
      }else if(499<=pos_x && pos_x<999 && 499<=pos_y && pos_y<=500){
         if(nodeId>car){
           roadD[4]-=2;
        }
        roadD[4]-=1;
      }else if(499<=pos_x && pos_x<=500 && 0<=pos_y && pos_y<499){
         if(nodeId>car){
           roadD[13]-=2;
        }
        roadD[13]-=1;
      }else if(499<=pos_x && pos_x<=500 && 499<=pos_y && pos_y<999){
         if(nodeId>car){
           roadD[17]-=2;
        }
        roadD[17]-=1;   
      }
  }else if(rid == 7){
      if(499<=pos_x && pos_x<999 && 499<=pos_y && pos_y<=500){
         if(nodeId>car){
           roadD[4]-=2;
        }
        roadD[4]-=1;
      }else if(999<=pos_x && pos_x<=1500 && 499<=pos_y && pos_y<=500){
         if(nodeId>car){
           roadD[5]-=2;
        }
        roadD[5]-=1;
      }else if(999<=pos_x && pos_x<=1000 && 0<=pos_y && pos_y<499){
         if(nodeId>car){
           roadD[14]-=2;
        }
        roadD[14]-=1;
      }else if(999<=pos_x && pos_x<=1000 && 499<=pos_y && pos_y<999){
         if(nodeId>car){
           roadD[18]-=2;
        }
        roadD[18]-=1;   
      }
  }else if(rid == 8){
      if(999<=pos_x && pos_x<1500 && 499<=pos_y && pos_y<=500){
         if(nodeId>car){
           roadD[5]-=2;
        }
        roadD[5]-=1;
      }else if(1499<=pos_x && pos_x<=1500 && 0<=pos_y && pos_y<499){
         if(nodeId>car){
           roadD[15]-=2;
        }
        roadD[15]-=1;
      }else if(1499<=pos_x && pos_x<=1500 && 499<=pos_y && pos_y<999){
         if(nodeId>car){
           roadD[19]-=2;
        }
        roadD[19]-=1;   
      }
  }else if(rid == 9){
      if(0<=pos_x && pos_x<499 && 999<=pos_y && pos_y<=1000){
         if(nodeId>car){
           roadD[6]-=2;
        }
        roadD[6]-=1;
      }else if(0<=pos_x && pos_x<=1 && 499<=pos_y && pos_y<999){
         if(nodeId>car){
           roadD[16]-=2;
        }
        roadD[16]-=1;
      }else if(0<=pos_x && pos_x<=1 && 999<=pos_y && pos_y<1500){
         if(nodeId>car){
           roadD[20]-=2;
        }
        roadD[20]-=1;   
      }
  }else if(rid == 10){
      if(0<=pos_x && pos_x<499 && 999<=pos_y && pos_y<=1000){
         if(nodeId>car){
           roadD[6]-=2;
        }
        roadD[6]-=1;
      }else if(499<=pos_x && pos_x<999 && 999<=pos_y && pos_y<=1000){
         if(nodeId>car){
           roadD[7]-=2;
        }
        roadD[7]-=1;
      }else if(499<=pos_x && pos_x<=500 && 499<=pos_y && pos_y<999){
         if(nodeId>car){
           roadD[17]-=2;
        }
        roadD[17]-=1;
      }else if(499<=pos_x && pos_x<=500 && 999<=pos_y && pos_y<1500){
         if(nodeId>car){
           roadD[21]-=2;
        }
        roadD[21]-=1;   
      }
  }else if(rid == 11){
      if(499<=pos_x && pos_x<999 && 999<=pos_y && pos_y<=1000){
         if(nodeId>car){
           roadD[7]-=2;
        }
        roadD[7]-=1;
      }else if(999<=pos_x && pos_x<1500 && 999<=pos_y && pos_y<=1000){
         if(nodeId>car){
           roadD[8]-=2;
        }
        roadD[8]-=1;
      }else if(999<=pos_x && pos_x<=1000 && 499<=pos_y && pos_y<999){
         if(nodeId>car){
           roadD[18]-=2;
        }
        roadD[18]-=1;
      }else if(999<=pos_x && pos_x<=1000 && 999<=pos_y && pos_y<1500){
         if(nodeId>car){
           roadD[22]-=2;
        }
        roadD[22]-=1;   
      }
  }else if(rid == 12){
      if(999<=pos_x && pos_x<1500 && 999<=pos_y && pos_y<=1000){
         if(nodeId>car){
           roadD[8]-=2;
        }
        roadD[8]-=1;
      }else if(1499<=pos_x && pos_x<=1500 && 499<=pos_y && pos_y<999){
         if(nodeId>car){
           roadD[19]-=2;
        }
        roadD[19]-=1;
      }else if(1499<=pos_x && pos_x<=1500 && 999<=pos_y && pos_y<1500){
         if(nodeId>car){
           roadD[23]-=2;
        }
        roadD[23]-=1;   
      }
  }else if(rid == 13){
      if(0<=pos_x && pos_x<499 && 1499<=pos_y && pos_y<=1500){
         if(nodeId>car){
           roadD[9]-=2;
        }
        roadD[9]-=1;
      }else if(0<=pos_x && pos_x<=1 && 999<=pos_y && pos_y<1500){
         if(nodeId>car){
           roadD[20]-=2;
        }
        roadD[20]-=1;   
      }
    }else if(rid == 14){
      if(0<=pos_x && pos_x<499 && 1499<=pos_y && pos_y<1500){
         if(nodeId>car){
           roadD[9]-=2;
        }
        roadD[9]-=1;
      }else if(499<=pos_x && pos_x<999 && 1499<=pos_y && pos_y<=1500){
         if(nodeId>car){
           roadD[10]-=2;
        }
        roadD[10]-=1;
      }else if(499<=pos_x && pos_x<=500 && 999<=pos_y && pos_y<1500){
         if(nodeId>car){
           roadD[21]-=2;
        }
        roadD[21]-=1;   
      }
  }else if(rid == 15){
      if(499<=pos_x && pos_x<999 && 1499<=pos_y && pos_y<=1500){
         if(nodeId>car){
           roadD[10]-=2;
        }
        roadD[10]-=1;
      }else if(999<=pos_x && pos_x<1500 && 1499<=pos_y && pos_y<=1500){
         if(nodeId>car){
           roadD[11]-=2;
        }
        roadD[11]-=1;
      }else if(999<=pos_x && pos_x<=1000 && 999<=pos_y && pos_y<1500){
         if(nodeId>car){
           roadD[22]-=2;
        }
        roadD[22]-=1;   
      }
  }else if(rid == 16){      
      if(999<=pos_x && pos_x<1500 && 1499<=pos_y && pos_y<=1500){
         if(nodeId>car){
           roadD[11]-=2;
        }
        roadD[11]-=1;
      }else if(1499<=pos_x && pos_x<=1500 && 999<=pos_y && pos_y<1500){
         if(nodeId>car){
           roadD[23]-=2;
        }
        roadD[23]-=1;
      } 
  }

 // std::cout<<sum<<"\n";

  //Time jitter = Time (Seconds (m_uniformRandomVariable->GetInteger (1, 2)));
  //Simulator::Schedule (jitter, &UchidaRoutingProtocol::SendHello, this);
}


void
UchidaRoutingProtocol::SendSDN()
{
  //RSUからSDNに送信する。
  //printf("SendSDN\n");
  //std::cout<<"In sendsdn(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
  //j != m_socketAddresses.end ()

  //ここのfor文でノードが持っているネットワークアドレス分だけ回している。消すことによって２回やっていたのが１回になった。
  //for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
    //{
   uint16_t id = m_ipv4->GetObject<Node> ()->GetId ();
    if(1<=id && id <=16){
      ucd =0;



    /*  
    if(m_ipv4->GetObject<Node> ()->GetId ()==1)
    for(int i=0;i<=23;i++){
      printf("%d:%d\n",i,roadD[i]);
    }
  */


      std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin ();
      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
      Vector pos = mobility->GetPosition ();
      SDNHeader sdnHeader;
      Ptr<Packet> packet = Create<Packet> ();
      sdnHeader.SetNodeId(m_ipv4->GetObject<Node> ()->GetId ());
      sdnHeader.SetPosition_X(pos.x);
      sdnHeader.SetPosition_Y(pos.y);
      packet->AddHeader(sdnHeader);

      TypeHeader tHeader (SDNTYPE);
      packet->AddHeader (tHeader);

      //uint16_t id =m_ipv4->GetObject<Node> ()->GetId ();

    Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        } else if(id == 1)
        {
           destination = Ipv4Address ("10.1.1.255");
        }else if(id == 2)
        {
           destination = Ipv4Address ("10.1.2.255");
        }else if(id == 3)
        {
           destination = Ipv4Address ("10.1.3.255");
        }else if(id == 4)
        {
           destination = Ipv4Address ("10.1.4.255");
        }else if(id == 5)
        {
           destination = Ipv4Address ("10.1.5.255");
        }else if(id == 6)
        {
           destination = Ipv4Address ("10.1.6.255");
        }else if(id == 7)
        {
           destination = Ipv4Address ("10.1.7.255");
        }else if(id == 8)
        {
           destination = Ipv4Address ("10.1.8.255");
        }else if(id == 9)
        {
           destination = Ipv4Address ("10.1.9.255");
        }else if(id == 10)
        {
           destination = Ipv4Address ("10.1.10.255");
        }else if(id == 11)
        {
           destination = Ipv4Address ("10.1.11.255");
        }else if(id == 12)
        {
           destination = Ipv4Address ("10.1.12.255");
        }else if(id == 13)
        {
           destination = Ipv4Address ("10.1.13.255");
        }else if(id == 14)
        {
           destination = Ipv4Address ("10.1.14.255");
        }else if(id == 15)
        {
           destination = Ipv4Address ("10.1.15.255");
        }else if(id == 16)
        {
           destination = Ipv4Address ("10.1.16.255");
        }


      Time jitter = Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0,1)));
      Simulator::Schedule (jitter, &UchidaRoutingProtocol::SendTo, this , socket, packet, destination);

  //  }  

  
    }
}

void
UchidaRoutingProtocol::RecvSDN(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
{
  //printf("RecvHello\n");
  //std::cout<<"In recvhello(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
  SDNHeader sdnHeader;
  packet->RemoveHeader(sdnHeader);
  uint16_t nodeId = sdnHeader.GetNodeId();
  //uint16_t pos_x = sdnHeader.GetPosition_X();
  //uint16_t pos_y = sdnHeader.GetPosition_Y();
  //printf("nodeId=%d\n",nodeId);

/*
  if(m_ipv4->GetObject<Node> ()->GetId () == 0){
     std::cout<<"受信者のID："<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
      std::cout<<"送信者のID："<<nodeId<<"送信した位置("<< pos_x<<","<<pos_y<<")\n";
      //std::cout<<"送信者のID："<<nodeId<<"\n";
  }  

  */    

  if(1<=nodeId && nodeId<=16){
    count++;
    //std::cout<<count<<"\n";
  }

  if(count==16){
  //int r=24; 
  //int a,b,l;
  
  int s=0;
  int d=15;

  /* 初期化 */
  for(int i = 0; i < SIZE; i++) {
    COST[i] = INF;
    USED[i] = FALSE;
    VIA[i] = -1;
    for(int j = 0; j < SIZE; j++)
      DIST[i][j] = INF;
  }

  /* ロードDを格納していく */
    DIST[0][1] = roadD[0];
    DIST[0][4] = roadD[12];
    
    DIST[1][2] = roadD[1];
    DIST[1][5] = roadD[13];

    DIST[2][3] = roadD[2];
    DIST[2][6] = roadD[14];

    DIST[3][7] = roadD[15];

    DIST[4][5] = roadD[3];
    DIST[4][8] = roadD[16];

    DIST[5][6] = roadD[4];
    DIST[5][9] = roadD[17];

    DIST[6][7] = roadD[5];
    DIST[6][10] = roadD[18];

    DIST[7][11] = roadD[19];

    DIST[8][9] = roadD[6];
    DIST[8][12] = roadD[20];

    DIST[9][10] = roadD[7];
    DIST[9][13] = roadD[21];

    DIST[10][11] = roadD[8];
    DIST[10][14] = roadD[22];

    DIST[11][15] = roadD[23];


    DIST[12][13] = roadD[9];
    DIST[13][14] = roadD[10];
    DIST[14][15] = roadD[11];


  /* ダイクストラ法で最短経路を求める */
  //printf("distance:%d\n", dijkstra(s,d));
  dijkstra(s,d);

  /* 経路を表示 */
  
  int node = d; 
  //printf("%d\n", node);
  
  //VIA[node]　多分次のノードのこと
  
  path[6]= node;

  //ゴールから順になっているのでそれを逆にして正順にしている。
  for(int i=5;i>=0;--i){
    node=VIA[node];
    path[i]=node;
    //printf("path[%d]は%d\n",i,path[i]);
  }
  
  //確認
  printf("*****PATH*****\n");
    for(int i=0;i<=6;i++){
    path[i]+=1;
    printf("%d\n",path[i]);
  }
  printf("***************\n");

    SendSTR();
  }


}

void
UchidaRoutingProtocol::SendSTR() //SDN to RSU
{
  //printf("SendSTR\n");
  //std::cout<<"In sendhello(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
  
  //for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
    //{

     //   printf("SendSTR\n");
     
      
      std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin ();
      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;

      Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
      Vector pos = mobility->GetPosition ();
      STRHeader strHeader;
      Ptr<Packet> packet = Create<Packet> ();
      strHeader.SetNodeId(m_ipv4->GetObject<Node> ()->GetId ());
      strHeader.SetPosition_X(pos.x);
      strHeader.SetPosition_Y(pos.y);
      packet->AddHeader(strHeader);

      TypeHeader tHeader (STRTYPE);
      packet->AddHeader (tHeader);

    Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        { 
          destination = Ipv4Address ("10.1.1.255");
        }

      //std::cout<<destination<<"\n";


        /*
        destinationをいじれば送りたい目的をいじることができるみたい
        else if(m_ipv4->GetObject<Node> ()->GetId ()==1)
        { 
          destination = Ipv4Address ("10.1.1.255");
          //destination = iface.GetBroadcast ();
        }

        destination = iface.GetBroadcast ();で全部！！！

        std::cout<<destination<<"\n";
        std::cout<<"送信者のID："<<m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
        */
      
       socket->SendTo (packet, 0, InetSocketAddress (destination, UCHIDA_PORT));
   // }  
}

void
UchidaRoutingProtocol::RecvSTR(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
{
  //printf("RecvHello\n");
  //std::cout<<"In recvhello(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
  STRHeader strHeader;
  packet->RemoveHeader(strHeader);
  //uint16_t nodeId = strHeader.GetNodeId();
  //uint16_t pos_x = strHeader.GetPosition_X();
  //uint16_t pos_y = strHeader.GetPosition_Y();
  //printf("nodeId=%d\n",nodeId);


  if(m_ipv4->GetObject<Node> ()->GetId () == 1){
    count=0;
    // std::cout<<"受信者のID："<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
      //std::cout<<"送信者のID："<<nodeId<<"送信した位置("<< pos_x<<","<<pos_y<<")\n";
      //std::cout<<"送信者のID："<<nodeId<<"\n";
      SendRTC();
  }     

}

void
UchidaRoutingProtocol::SendRTC() //RSUtoCar(SRC)
{
   //printf("SendRTC\n");
      
      std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin ();
      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;

  
      RTCHeader rtcHeader;
      Ptr<Packet> packet = Create<Packet> ();
      packet->AddHeader(rtcHeader);

      TypeHeader tHeader (RTCTYPE);
      packet->AddHeader (tHeader);

    Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        { 
          destination = Ipv4Address ("10.1.17.255");
        }
   
       socket->SendTo (packet, 0, InetSocketAddress (destination, UCHIDA_PORT));

}

void
UchidaRoutingProtocol::RecvRTC(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
{
  if(m_ipv4->GetObject<Node> ()->GetId () != 17){
    return;
  }
 
  //std::cout<<"In recvhello(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
  RTCHeader rtcHeader;
  packet->RemoveHeader(rtcHeader);
  
  //printf("ababababababbabababbab\n");
  SendBroadcast();

}


void
UchidaRoutingProtocol::SendBroadcast()
{
   //printf("SendBroadcast\n");
     
  for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
    {
     
     //printf("%d\n",sendcount);

 
      
      if(sendcount == 2){
      sendcount = 0;
      memset(recvcount, 0, sizeof(recvcount));
      cb2=0;       
      }
      
      
       if(m_ipv4->GetObject<Node> ()->GetId ()==17 && sendcount==0){
       printf("\n");
       printf("SRC:send packet\n");
       printf("\n");
       //start = std::chrono::system_clock::now(); 
       ST= Simulator::Now ().GetMilliSeconds ();
       
       }

      sendcount++;


      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
      Vector pos = mobility->GetPosition ();
      BROADCASTHeader broadcastHeader;
      Ptr<Packet> packet = Create<Packet> ();
      broadcastHeader.SetNodeId(m_ipv4->GetObject<Node> ()->GetId ());
      broadcastHeader.SetPosition_X(pos.x);
      broadcastHeader.SetPosition_Y(pos.y);
      packet->AddHeader(broadcastHeader);

      TypeHeader tHeader (BROADCASTTYPE);
      packet->AddHeader (tHeader);

    Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        { 
          destination = Ipv4Address("10.1.19.255");
        }

      //std::cout<<destination<<"\n";

      socket->SendTo (packet, 0, InetSocketAddress (destination, UCHIDA_PORT));

      //Time jitter = Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0,10)));
      //Simulator::Schedule (jitter, &UchidaRoutingProtocol::SendTo, this , socket, packet, destination);

    }
}


void
UchidaRoutingProtocol::RecvBroadcast(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
{
  BROADCASTHeader broadcastHeader;
  packet->RemoveHeader(broadcastHeader);

 uint16_t rid = m_ipv4->GetObject<Node> ()->GetId ();


 if(rid<=17){
   return;
 }
  uint16_t nodeId = broadcastHeader.GetNodeId();
  uint16_t pos_x = broadcastHeader.GetPosition_X();
  uint16_t pos_y = broadcastHeader.GetPosition_Y();


 //ここから250m以上で普通車の通信をやめさせるようにしてる
   
  Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
  Vector mypos = mobility->GetPosition ();
  uint16_t rpos_x;
  uint16_t rpos_y;
  rpos_x = mypos.x;
  rpos_y = mypos.y;
  double tmp = rpos_x;
  double tmp2 =rpos_y;
  tmp -= pos_x;
  tmp2 -= pos_y;
  
  tmp=tmp*tmp;
  tmp2=tmp2*tmp2;

  double tmp3 = tmp +tmp2;
  double tmp4 = sqrt(tmp3);
  if(tmp4>=250 && rid>16 && nodeId<=car){
  return;
  }
   if(tmp4>=375 && rid<=car && nodeId>car){
    return;
  }

 if(recvcount[rid]>1){
   return; 
  }


  //printf("RecvBroadcast\n");
  

  if(path[1]==2){
   if(rpos_x<=500 && 0<=rpos_y && rpos_y <=1){
     //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path=2"<<"\n";
     //std::cout<<"Recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
     recvcount[rid]++;
     SendBroadcast2(1);
   }
  }else if(path[1]==5){
    if(0<=rpos_x && rpos_x<=1 && rpos_y <=500){
      //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path=5"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
       recvcount[rid]++;
     SendBroadcast2(1);
   }
  }
    
}


void
UchidaRoutingProtocol::SendBroadcast2(uint8_t hop)
{
   //printf("SendBroadcast2\n");
  //std::cout<<"In sendbroadcast2(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";
    
    std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin ();
    Ptr<Socket> socket = j->first;
    Ipv4InterfaceAddress iface = j->second;


      Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
      Vector pos = mobility->GetPosition ();
      BROADCAST2Header broadcast2Header;
      Ptr<Packet> packet = Create<Packet> ();
      broadcast2Header.SetNodeId(m_ipv4->GetObject<Node> ()->GetId ());
      broadcast2Header.SetPosition_X(pos.x);
      broadcast2Header.SetPosition_Y(pos.y);
      
      broadcast2Header.SetHopCount(hop);


      packet->AddHeader(broadcast2Header);

      TypeHeader tHeader (BROADCAST2TYPE);
      packet->AddHeader (tHeader);

    Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        { 
          destination = Ipv4Address ("10.1.19.255");
        }

      cb2++;

      Time jitter = Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0,10)));
      Simulator::Schedule (jitter, &UchidaRoutingProtocol::SendTo, this , socket, packet, destination);

    }


int packetC = 0;   //パケットが届いた数
uint32_t delayC=0; //遅延の合計（平均値算出用）
int hopC =0; //ホップ数の総数（平均値算出用）

void
UchidaRoutingProtocol::RecvBroadcast2(Ptr<Packet> packet, Ipv4Address receiver, Ipv4Address sender)
{
  BROADCAST2Header broadcast2Header;
  packet->RemoveHeader(broadcast2Header);

 

  // //std::cout<<"In recvbroadcast2(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";

 uint16_t rid = m_ipv4->GetObject<Node> ()->GetId ();

  

 if(recvcount[rid]>1){
  return;
}

  uint16_t nodeId = broadcast2Header.GetNodeId();
  uint16_t pos_x = broadcast2Header.GetPosition_X();
  uint16_t pos_y = broadcast2Header.GetPosition_Y();

 //ここから250m以上で普通車の通信をやめさせるようにしてる
   
  Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> ()->GetObject<MobilityModel>();
  Vector mypos = mobility->GetPosition ();
  uint16_t rpos_x;
  uint16_t rpos_y;
  rpos_x = mypos.x;
  rpos_y = mypos.y;
  double tmp = rpos_x;
  double tmp2 =rpos_y;
  tmp -= pos_x;
  tmp2 -= pos_y;
  
  tmp=tmp*tmp;
  tmp2=tmp2*tmp2;

  double tmp3 = tmp +tmp2;
  double tmp4 = sqrt(tmp3);

   if(tmp4>=250 && rid>16 && nodeId<=car){
    return;
  }

  if(tmp4>=375 && rid<=car && nodeId>car){
    return;
  }


    
if(rid==18 && recvcount[rid]==0){
    
     RT= Simulator::Now ().GetMilliSeconds ();
    uint32_t elapsed = RT-ST;
    
    

    recvcount[rid]++;

    uint16_t lasthop = broadcast2Header.GetHopCount();
    lasthop = lasthop + 1;

    packetC++;
    delayC+=elapsed;
    hopC+=lasthop;

    printf("-------------------\n");

    //std::cout<<"In recvbroadcast2(Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<")\n";

    printf("packet arrived DST\n");
    printf("path:");
    for(int i=0;i<6;i++){
      printf("%d->",path[i]);
    }
    printf("16\n");
    std::cout<<"送信元(Node "<< nodeId<<")\n";
    std::cout<<"遅延"<< elapsed<<"ms\n";
    std::cout<<"hop数:"<<lasthop<<"回\n";
     std::cout<<"総ブロードキャスト数"<<cb2<<"回\n";
    printf("-------------------\n");
    return;
  }


uint16_t hop = broadcast2Header.GetHopCount();
  
if(recvcount[18]==0){
 if(path[2]==3){
 if(250<rpos_x && rpos_x<999 && 0<=rpos_y && rpos_y<=1){
   //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
   //std::cout<<"path[2]=3"<<"\n";
   //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
   recvcount[rid]++;
    SendBroadcast2(hop+1);
   }
 }
 if(path[1]==2 && path[2]==6){
   if(rpos_x<=500 && 0<=rpos_y && rpos_y<499){
     //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path1=2 path2=6"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
    recvcount[rid]++;
     SendBroadcast2(hop+1);
    }
 }
  if(path[1]==5 && path[2]==6){
    if(250<=pos_y && pos_y<=500 && 0<=rpos_x && rpos_x <499){
     //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path1=5 path2=6"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
    recvcount[rid]++;
    
     SendBroadcast2(hop+1);
     }
  }
 if(path[2]==9){
    if(0<=rpos_x && rpos_x<=1 && 250<rpos_y && rpos_y <999){
      //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path2=9"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
     recvcount[rid]++;
     
     SendBroadcast2(hop+1);
   }
  }

 if(path[3]==4){
   if(750<rpos_x && rpos_x<=1500 && rpos_y >=0 && rpos_y<=1){
    //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path3=4"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
     recvcount[rid]++;
     SendBroadcast2(hop+1);
   }
  }else if(path[2]==3 && path[3]==7){
    if(750 <pos_x && pos_x<=1000 && 0<=rpos_y && rpos_y <499){
      //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path2=3 path3=7"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
        recvcount[rid]++;
        
     SendBroadcast2(hop+1);
    }
   }else if(path[2]==6 && path[3]==7){
    if(250<pos_y && pos_y<=500 && 250<rpos_x && rpos_x <=998){
      //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path2=6 path3=7"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
        recvcount[rid]++;
        
     SendBroadcast2(hop+1);
   }
  }else if(path[2]==6 && path[3]==10){
    if(250<rpos_x && rpos_x<=500 && 250<rpos_y && rpos_y <999){
     //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path2=6 path3=10"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
          recvcount[rid]++;
          
     SendBroadcast2(hop+1);
    }
   }else if(path[2]==9 && path[3]==10){
    if(750<rpos_y && rpos_y<=1000 && 0<=rpos_x && rpos_x <499){
     //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path2=9 path3=10"<<")\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
          recvcount[rid]++;
          
     SendBroadcast2(hop+1);
   }
  }else if(path[3]==13){
    if(rpos_x>=0 && rpos_x<=1 && 750<rpos_y && rpos_y <=1500){
     //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path3=13"<<")\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
    
          recvcount[rid]++;
          
     SendBroadcast2(hop+1);

    }
  }

   if(path[3]==4 && path[4]==8){
   if(750<rpos_x && rpos_x<=1500 && 0<=rpos_y && pos_y<499){
     //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[3]==4 && path[4]==8"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
 
     recvcount[rid]++;
     
     SendBroadcast2(hop+1);
   }
  }else if(path[3]==7 && path[4]==8){
    if(750 <pos_x && pos_x<1499 && 250<rpos_y && rpos_y <=500){
       //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[3]==7 && path[4]==8"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
 
     recvcount[rid]++;
     
     SendBroadcast2(hop+1);
    }
   }else if(path[3]==7 && path[4]==11){
    if(750<pos_x && pos_x<=1000 && 250<rpos_y && rpos_y <999){
       //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[3]==7 && path[4]==11"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
        recvcount[rid]++;
        
     SendBroadcast2(hop+1);
   }
  }else if(path[3]==10 && path[4]==11){
    if(250<rpos_x && rpos_x<999 && 750<rpos_y && rpos_y <=1000){
       //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[3]==10 && path[4]==11"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
 
          recvcount[rid]++;
          
     SendBroadcast2(hop+1);
    }
   }else if(path[3]==10 && path[4]==14){
    if(250<rpos_x && rpos_x<=500 && 750<=rpos_x && rpos_y <1499){
       //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[3]==10 && path[4]==14"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
 
     recvcount[rid]++;
          
     SendBroadcast2(hop+1);
   }
  }else if(path[3]==13 && path[4]==14){
    if(0<=rpos_x && rpos_x<499 && 750<rpos_y && rpos_y <=1500){
       //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[3]==13 && path[4]==14"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
 
     recvcount[rid]++;
     
     SendBroadcast2(hop+1);
    }
  }

 if(path[4]==8 && path[5]==12){
   if(750<rpos_x && rpos_x<=1500 && 251<=rpos_y && pos_y<999){
      //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[4]==8 && path[5]==12"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
 
     recvcount[rid]++;
     
     SendBroadcast2(hop+1);
   }
  }else if(path[4]==11 && path[5]==12){
    if(750 <pos_x && pos_x<1499 && 750<rpos_y && rpos_y <=1000){
       //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[4]==11 && path[5]==12"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
 
     recvcount[rid]++;
     
     SendBroadcast2(hop+1);
    }
   }else if(path[4]==11 && path[5]==15){
    if(750<pos_x && pos_x<=1000 && 750<rpos_y && rpos_y <1499){
       //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[4]==11 && path[5]==15"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
 
        recvcount[rid]++;
        
     SendBroadcast2(hop+1);
   }
  }else if(path[4]==14 && path[5]==15){
    if(250<rpos_x && rpos_x<999 && 1250<rpos_y && rpos_y <=1500){
       //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[4]==14 && path[5]==15"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
 
     recvcount[rid]++;
     
     SendBroadcast2(hop+1);
    }
   }

    if(path[5]==12 && path[6]==16){
   if(1250<rpos_x && rpos_x<=1500 && 750<=rpos_y && pos_y<1499){
      //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[5]==12 && path[6]==16"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";
 
     recvcount[rid]++;
     SendBroadcast2(hop+1);
   }
  }else if(path[5]==15 && path[6]==16){
    if(750 <pos_x && pos_x<1499 && 1250<rpos_y && rpos_y <=1500){
       //printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
     //std::cout<<"path[5]==15 && path[5]==16"<<"\n";
     //std::cout<<"In recvbroadcast:Node "<< m_ipv4->GetObject<Node> ()->GetId ()<<"("<<rpos_x<<","<<rpos_y<<")\n";

     recvcount[rid]++;
     
     SendBroadcast2(hop+1);
    }
   }
}

}



void 
UchidaRoutingProtocol::SimulationResult (void)
{

printf("ーーーーーーー最終結果ーーーーーーーーーーー\n");

printf("総パケット送信数::20回\n");

double send =20;
double packetAve = packetC/send; //パケット到達率
double delayAve = delayC/packetC; //平均遅延
double hopAve = hopC/packetC; //平均hop数
printf("パケット到達回数::%d\n",packetC);
printf("パケット到達率::%f\n",packetAve);
printf("平均遅延::%f(ms)\n",delayAve);
printf("平均hop数%f回\n",hopAve);

printf("ーーーーーーーーーーーーーーーーーーーーーー\n");
}





} //namespace uchida
} //namespace ns3
